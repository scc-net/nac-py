#! /usr/bin/env python3

import radiusd

import re
import random
import psycopg2
import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from psycopg2.extras import RealDictCursor
from functools import wraps
import sys
import traceback
import smtplib
import email.utils
import uuid
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import redis


### utility functions ###
def format_mac(mac):
  mac = mac.lower().strip()
  mac = re.sub(r'(.*?)([0-9a-f]+|$|^)', lambda m: ('' * len(m.groups()[0]) + m.groups()[1]), mac)
  return ''.join(':' + c if i % 2 == 0 and not i == 0 else c for i, c in enumerate(mac, 0))


def usable_freeradius_function(func):
  @wraps(func)
  def wrapper(*args, **kwargs):
    try:
      return func({i[0]: i[1] for i in args[0]})
    except:
      print(traceback.format_exc())
      return radiusd.RLM_MODULE_INVALID
  return wrapper

  
def load_db_config():
  with open('/etc/macauth/db_config.yml', 'r') as fhandle:
    return yaml.load(fhandle, Loader=Loader)

def load_accounting_config():
  with open('/etc/macauth/nac_config.yml', 'r') as fhandle:
    return yaml.load(fhandle, Loader=Loader)

def get_db_conn(config, accounting=False):
  scope = 'accounting_db' if accounting else 'auth_db'
  connect_string = "dbname='%s' user='%s' host='%s' password='%s'" % (config[scope]['db'], config[scope]['user'], config[scope]['host'], config[scope]['password'])
  return psycopg2.connect(connect_string, cursor_factory=RealDictCursor)

def get_redis_conn(config):
  connect_string = config['redis_cache']['url']
  return redis.from_url(connect_string)

def send_mail(config, accounting_group, mac, device, nas_port, to):
  conf = config['mailing']
  text = MIMEText(conf['body'].format(mac=mac, device=device, accounting_group=accounting_group, nas_port=nas_port),
            'plain', 'utf-8')
  msg = MIMEMultipart()
  msg.attach(text)
  msg['Subject'] = conf['subject'].format(accounting_group=accounting_group, mac=mac)
  msg['To'] = "<" + to + ">"
  msg['Reply-To'] = '<' + conf['reply_to'] + '>'
  msg['Return-Path'] = '<' + conf['reply_to'] + '>'
  msg['From'] =  '<' + conf['from'] + '>'
  msg['Date'] = email.utils.formatdate(localtime=True)
  msg['Message-ID'] = '<' + str(uuid.uuid4()) + '@' + conf['from'].split('@')[-1] + '>'
  s = smtplib.SMTP(conf['smarthost'])
  try:
    s.starttls()
    s.sendmail(conf['from'], to, msg.as_string())
  finally:
    s.quit()

def resolve_device(conn, ip):
  with conn.cursor() as cur:
    cur.execute("""
SELECT name from nd_dev
  inner join dns_ntree on nd_dev.dns_ntree_key_nr = dns_ntree.key_nr
  inner join dns_rr_owner on dns_rr_owner.dns_ntree_key_nr = dns_ntree.key_nr
  inner join dns_rr_set on dns_rr_set.dns_rr_owner_key_nr = dns_rr_owner.key_nr
  inner join dns_rr_dst on dns_rr_dst.dns_rr_set_key_nr = dns_rr_set.key_nr
  inner join dns_addr on dns_rr_dst.dns_addr_key_nr = dns_addr.key_nr
WHERE dns_addr.ip_addr = %s
""", (ip,))
    res = cur.fetchone()
    if res is None:
      return None
    return res['name']

def reject(user_name, nas, redis_conn):
  #cache = { 'state': 'reject' }
  #pipe = redis_conn.pipeline()
  #pipe.hset(f'{nas}${user_name}', mapping=cache).expire(f'{nas}${user_name}', 60+random.randint(-10,10)).execute()
  return (radiusd.RLM_MODULE_OK, tuple(), (('Auth-Type', 'Reject'),))


### functions called by freeradius ###

def post_auth_accept(p):
  db_conf = load_db_config()
  with get_db_conn(db_conf, accounting=True) as db_conn:
    with db_conn.cursor() as cur:
      nas_ip_addr = None
      device = None
      if db_conf['global']['mpsk_mode']:
        device = p.get('Aruba-Location-Id', None)
      else:
        nas_ip_addr = p.get('NAS-IPv6-Address', p.get('NAS-IP-Address', None))
        device = resolve_device(db_conn, nas_ip_addr)
      cur.execute("UPDATE macauth_client SET last_login_date = NOW(), last_login_node_info = %s WHERE mac_addr = %s", ((device if device is not None else 'unknown device') + ' ' + str(p.get('NAS-Port-Id', "")), p['User-Name']))


@usable_freeradius_function
def post_auth_reject(p):
  nas_ip_addr = p.get('NAS-IPv6-Address', p.get('NAS-IP-Address', None))
  user_name = format_mac(p['User-Name'])
  conf = load_db_config()
  redis_conn = get_redis_conn(conf)
  cached_resp = redis_conn.hgetall(f'{nas_ip_addr}${user_name}')
  if cached_resp is not None and len(cached_resp) > 0:
    # For now we only care about rejcts (negative cache for burst prevention)
    if cached_resp['state'] == 'reject':
      return radiusd.RLM_MODULE_OK
  cache = { 'state': 'reject' }
  pipe = redis_conn.pipeline()
  pipe.hset(f'{nas_ip_addr}${user_name}', mapping=cache).expire(f'{nas_ip_addr}${user_name}', 60+random.randint(-10,10)).execute()
  if conf['global']['mpsk_mode']:
    return radiusd.RLM_MODULE_OK
  conf_accounting = load_accounting_config()
  accounting_groups = conf_accounting['accounting_groups']
  with get_db_conn(conf) as db_conn:
    res = resolve_device(db_conn, nas_ip_addr)
    if res is None:
      radiusd.radlog(radiusd.L_WARN, 'could not resolve NAS-IP-Address in netdoc!')
      return radiusd.RLM_MODULE_OK
    device = res
    matched_groups = []
    for (name, g) in accounting_groups.items():
      for dev in g:
        if dev == device:
          matched_groups.append(name)
    if len(matched_groups) == 0:
      return radiusd.RLM_MODULE_OK
    for g in matched_groups:
      if not conf_accounting['accounting'][g].get('send_mail_on_reject', False):
        continue
      for m in conf_accounting['accounting'][g]['contact_mails']:
        send_mail(conf_accounting, mac=user_name, device=device, accounting_group=g, nas_port=p.get('NAS-Port-Id', p.get('NAS-Port', 'Unknown')), to=m)
  return radiusd.RLM_MODULE_OK


@usable_freeradius_function
def authorize(p):
  user_name = format_mac(p['User-Name'])
  nas_ip_addr = p.get('NAS-IPv6-Address', p.get('NAS-IP-Address', None))
  db_config = load_db_config()
  redis_conn = get_redis_conn(db_config)
  cached_resp = redis_conn.hgetall(f'{nas_ip_addr}${user_name}')
  if cached_resp is not None and len(cached_resp) > 0:
    # For now we only care about rejcts (negative cache for burst prevention)
    if cached_resp['state'] == 'reject':
      radiusd.radlog(radiusd.L_WARN, f'{user_name} via {nas_ip_addr}: Rejected from redis cache.')
      return (radiusd.RLM_MODULE_OK, tuple(), (('Auth-Type', 'Reject'),))
  with get_db_conn(db_config) as db_conn:
    with db_conn.cursor() as cur:
      if not db_config['global']['mpsk_mode']:
        cur.execute("""
SELECT macauth_client.wpa_key, macauth_client.mac_addr, nv.id as vlan_id, dns_addr.ip_addr as dev_ip_addr FROM macauth_client
  inner join nd_bcd nb on macauth_client.nd_bcd_key_nr = nb.key_nr
  inner join nd_vlan nv on nb.key_nr = nv.nd_bcd_key_nr
  inner join nd_vlan2dev nv2d on nv.key_nr = nv2d.nd_vlan_key_nr
  inner join nd_dev dev on nv2d.nd_dev_key_nr = dev.key_nr
  inner join dns_ntree on dev.dns_ntree_key_nr = dns_ntree.key_nr
  inner join dns_rr_owner on dns_rr_owner.dns_ntree_key_nr = dns_ntree.key_nr
  inner join dns_rr_set on dns_rr_set.dns_rr_owner_key_nr = dns_rr_owner.key_nr
  inner join dns_rr_dst on dns_rr_dst.dns_rr_set_key_nr = dns_rr_set.key_nr
  inner join dns_addr on dns_rr_dst.dns_addr_key_nr = dns_addr.key_nr
WHERE mac_addr = %s and dns_addr.ip_addr = %s""", (user_name, nas_ip_addr))
      else:
        cur.execute("""
SELECT macauth_client.wpa_key, macauth_client.mac_addr, nv.id as vlan_id FROM macauth_client
  inner join nd_bcd nb on macauth_client.nd_bcd_key_nr = nb.key_nr
  inner join nd_vlan nv on nb.key_nr = nv.nd_bcd_key_nr
  inner join nd_net_instnc nni on nv.nd_net_instnc_key_nr = nni.key_nr
WHERE mac_addr = %s and (nni.name='service_1_cn+cs' or nni.name='global_kit')""", (user_name,))
      res = cur.fetchall()
      if len(res) > 1 or len(res) == 0:
        if len(res) > 1:
          radiusd.radlog(radiusd.L_WARN, 'ambiguous database result (are multiple vlans of the same BCD configured on the switch? this should never happen in MPSK mode!)')
        else:
          if db_config['global']['mpsk_mode']:
            return reject(user_name, nas_ip_addr, redis_conn) 
          res = resolve_device(db_conn, nas_ip_addr)
          if res is None:
            radiusd.radlog(radiusd.L_WARN, 'could not resolve NAS-IP-Address in netdoc!')
            return reject(user_name, nas_ip_addr, redis_conn) 
          device = res
          conf_accounting = load_accounting_config()
          matched_groups = []
          accounting_groups = conf_accounting['accounting_groups']
          for (name, g) in accounting_groups.items():
            for dev in g:
              if dev == device:
                matched_groups.append(name)
          if len(matched_groups) == 0:
            return reject(user_name, nas_ip_addr, redis_conn) 
          for g in matched_groups:
            allowed_prefixes = conf_accounting['accounting'][g].get('accept_prefixes', [])
            for p in allowed_prefixes:
              if user_name.startswith(p['prefix']):
                reply = tuple()
                if 'vlan' in p:
                  reply = (('Tunnel-Private-Group-Id', str(p['vlan'])), ('Tunnel-Type', 'VLAN'), ('Tunnel-Medium-Type', 'IEEE-802'))
                if 'reply_additional' in p:
                  reply += tuple((str(k),str(v)) for k,v in p['reply_additional'].items())
                config = (('Auth-Type', 'Accept'),)
                radiusd.radlog(radiusd.L_INFO, 'accepting based on configured prefix')
                return (radiusd.RLM_MODULE_OK, reply, config)
        return reject(user_name, nas_ip_addr, redis_conn)
      res = res[0]
      reply = None
      if not db_config['global']['mpsk_mode']:
        reply = (('Tunnel-Private-Group-Id', str(res['vlan_id'])), ('Tunnel-Type', 'VLAN'), ('Tunnel-Medium-Type', 'IEEE-802'))
      else:
        reply = (('Tunnel-Private-Group-Id', str(res['vlan_id'])), ('Tunnel-Type', 'VLAN'), ('Tunnel-Medium-Type', 'IEEE-802'), ('Aruba-MPSK-Passphrase', str(res['wpa_key'])))
      config = (('Auth-Type', 'Accept'),)
      post_auth_accept(p)
      return (radiusd.RLM_MODULE_OK, reply, config)
  return radiusd.RLM_MODULE_INVALID
